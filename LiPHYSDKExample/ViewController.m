//
//  ViewController.m
//  LiPHYSDKExample
//
//  Created by Jetcomm on 5/15/18.
//  Copyright © 2018 Jetcomm. All rights reserved.
//

#import "ViewController.h"
#import <LightFlySDK/LightFlySDK.h>

@interface ViewController () <LFLightManagerDelegate>
@property (weak, nonatomic) IBOutlet UILabel *lightIDnumber;
@property (weak, nonatomic) IBOutlet UILabel *sdkVersionLabel;
@property (weak, nonatomic) IBOutlet UILabel *expiryDateLabel;
@property (weak, nonatomic) IBOutlet UISegmentedControl *switchCamera;

@property (nonatomic, strong) LFLightManager *lightManager;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.lightManager = [[LFLightManager alloc] init];
    self.sdkVersionLabel.text = self.lightManager.sdkVersion;
    self.lightManager.delegate = self;
    //self.lightManager.activationKey = @"S$[tSf)8Dc(0z';$[/%dk4X~s-*LASP>" // Exp March 2020
    ;
    
    [self.lightManager startTracking];
    NSLog(@"A-Expire Date:%@",self.lightManager.expiryDate);
    self.expiryDateLabel.text = self.lightManager.expiryDate;

}

- (IBAction)cameraSwitched:(id)sender {
    if(self.switchCamera.selectedSegmentIndex == 0) // FrontSide
    {
        self.lightManager.stopTracking;
        self.lightManager.isFront = YES;
        self.lightManager.startTracking;
    }
    else // BackCamera
    {
        self.lightManager.stopTracking;
        self.lightManager.isFront = NO;
        self.lightManager.startTracking;
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)lightManager:(LFLightManager *)manager didTrackLight:(LFLight *)light {
    dispatch_async(dispatch_get_main_queue(), ^{
    
        self.lightIDnumber.text = light.identifier;
        
    });
                   
}

- (void)lightManager:(LFLightManager *)manager didUpdateProgress:(float)progress {
    dispatch_async(dispatch_get_main_queue(), ^{
    
        NSLog(@"Progress is:%f",progress);
        
    });
                   
}

@end
