//
//  AppDelegate.h
//  LiPHYSDKExample
//
//  Created by Jetcomm on 5/15/18.
//  Copyright © 2018 Jetcomm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

