//
//  main.m
//  LiPHYSDKExample
//
//  Created by Jetcomm on 5/15/18.
//  Copyright © 2018 Jetcomm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
