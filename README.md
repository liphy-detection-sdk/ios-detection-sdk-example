# Installation

1. Extract the SDK to a folder named LightFlySDK under your project root
2. Add below line to your Podfile
    ```
    pod 'LightFlySDK', :path => './LightFlySDK'
    ```
3. Run below command in your project root.
    ```
    pod install
    ```
4. Add NSCameraUsageDescription to your Info.plist for gaining access to iOS camera



# Usage

```
self.lightManager = [[LFLightManager alloc] init]; self.lightManager.isFront = YES;
self.lightManager.delegate = self; self.lightManager.activationKey = @”axscbhawe873d0asc70382”;
[self.lightManager startTracking];

- (void)lightManager:(LFLightManager *)manager didTrackLight:(LFLight *)light {
    // Do the light detection related events here
}
```



# Classes and protocols

## LFLight
- A Class that represents a VLC light along with identifier information. Identifier is a unique ID associate with each light that can be used to trigger access to a certain URI, just like a QR code.- 
    ```
    // Declaration
    
    // OBJECTIVE-C
    @property (readonly, nonatomic) NSString *identifier;
    // SWIFT
    var identifier: String! { get }
    ```

## LFLightManager
- The LFLightManager object is the entry point to the VLC light service.
- `delegate`: Specifies the delegate to receive tracking callbacks.  
    ```
    // Declaration
    
    // OBJECTIVE-C
    @property (readwrite, nonatomic) id<LFLightManagerDelegate> delegate;
    // SWIFT
    weak var delegate: LFLightManagerDelegate! { get set }
    ```

- `isTracking`: Returns YES if camera is enabled to track VLC lights.
    ```
    // Declaration
    
    // OBJECTIVE-C
    @property (readonly, nonatomic) BOOL isTracking;
    SWIFT
    var isTracking: Int32 { get }
    ```

- `isFront`: To choose between front and back camera. By default, this is NO and -startTracking will initiate an AVCaptureSession for rear camera. Setting this property to YES will require calling -startTracking again in order to take effect.

    ```
    // Declaration
    
    // OBJECTIVE-C
    @property (assign, readwrite, nonatomic) BOOL isFront;
    // SWIFT
    var isFront: Int32 { get set }
    ```

- `startTracking`: Start tracking VLC light. The camera defined in isFront will be used.
    ```
    // Declaration
    
    // OBJECTIVE-C
    - (void)startTracking;
    // SWIFT
    func startTracking()
    ```

- `stopTracking`: To stop tracking the VLC Lights.
    ```
    // Declaration
    
    // OBJECTIVE-C
    - (void)stopTracking;
    // SWIFT
    func stopTracking()
    ```


## LFLightManagerDelegate
- This is the delegate for LFLightManager
- `lightManager:didTrackLight:` This is invoked when a VLC light is in the vision of camera.
    ```
    // Declaration
    
    // OBJECTIVE-C
    - (void)lightManager:(LFLightManager *)manager didTrackLight:(LFLight *)light;
    // SWIFT
    func lightManager(_ manager: LFLightManager!, didTrackLight light: LFLight!)
    ```
